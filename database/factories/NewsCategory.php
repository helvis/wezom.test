<?php

use Faker\Generator as Faker;

$factory->define(App\Models\NewsCategory::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'is_hidden' => rand(0, 1),
    ];
});

<?php

use App\Models\NewsCategory;
use App\User;
use Faker\Generator as Faker;

$factory->define(App\Models\News::class, function (Faker $faker) {
    $categoryIds = NewsCategory::pluck('id', 'id')->all();

    $userIds = User::pluck('id', 'id')->all();

    return [
        'category_id' => array_rand($categoryIds),
        'subject' => $faker->sentence,
        'published_at' => $faker->date('Y-m-d H:i:s'),
        'is_hidden' => rand(0, 1),
        'content' => $faker->text(800),
        'views' => rand(10, 999),
        'creator_id' => array_rand($userIds),
        'image' => '',
    ];
});

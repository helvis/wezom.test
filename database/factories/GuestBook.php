<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 22/02/18
 * Time: 12:53
 */

use Faker\Generator as Faker;

$factory->define(App\Models\GuestBook::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'email' => $faker->email,
        'text' => $faker->text(400),
    ];
});
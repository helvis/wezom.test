<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('news_categories');

            $table->string('subject');
            $table->text('content');
            $table->string('image')->nullable();

            $table->integer('is_hidden')->default(0);

            $table->integer('views')->default(0);

            $table->integer('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('users');

            $table->timestamp('published_at');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('news');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 22/02/18
 * Time: 12:57
 */

use App\Models\GuestBook;
use Illuminate\Database\Seeder;


class GuestBookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(GuestBook::class, 50)
            ->create();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 19/02/18
 * Time: 20:11
 */

use App\User;
use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    public function run()
    {
        factory(User::class, 5)->create();
    }
}
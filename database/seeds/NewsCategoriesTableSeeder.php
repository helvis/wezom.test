<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 19/02/18
 * Time: 20:14
 */

use App\Models\NewsCategory;
use Illuminate\Database\Seeder;

class NewsCategoriesTableSeeder extends Seeder
{
    public function run()
    {
        factory(NewsCategory::class, 10)
            ->create();
    }
}
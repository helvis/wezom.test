### Системные требования:
PHP > 7.0
php_gd
php_curl
php_fileinfo
php_mbstring

composer
vagrant

### Установка :
Клонируем из репозитория и переходим в каталог:
```
git clone https://bitbucket.org/helvis/wezom.test.git test
cd test
```
Устанавливаем зависимости:
```
composer install --no-scripts
```
Инициализируем конфигурационный файл Homestead:
```
vendor\\bin\\homestead make
```
Настроек по умолчанию должно хватить.
Запускаем Vagrant:
```
vagrant up
```

### Настройка
Заходим на виртуальную машину через ssh:
```
vagrant ssh
```

Переходим в каталог с дистрибутивом:
```
cd ~/code/
```
Создаем конфигурационный файл .env, из .env.example
```
cp .env.example .env
```
Генерируем ключ:
```
php artisan key:generate
```
Выполняем миграции:
```
php artisan migrate
```
Создаем линк в каталог для загрузки файлов:
```
php artisan storage:link
```
Если надо заполнить таблицы данными для демонстрации, выполняем :
```
php artisan db:seed
```
Теперь нужно поправить файл hosts. Добавляем туда строку соответствия IP и имени хоста:
```
192.168.10.10   homestead.test
```
Если на этапе генерации конфигурации Homestead мы меняли в нём адрес и имя хоста, то предыдущая строка должна выглядить с учетом измененных настроек.
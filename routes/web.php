<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\GuestBookController;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/show/{id}', 'HomeController@show')->name('home.show');

Route::get('/guest-book', 'GuestBookController@index')->name('guest-book');
Route::get('/guest-book/create', 'GuestBookController@create')->name('guest-book.create');
Route::post('/guest-book/store', 'GuestBookController@store')->name('guest-book.store');

Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
    return $captcha->src($config);
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => AdminMiddleware::class
],
    function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::resource('/news', 'NewsController', ['except' => ['show']]);
        Route::resource('/categories', 'NewsCategoryController', ['except' => ['show']]);
        Route::resource(
            '/guest-book',
            'GuestBookController',
            [
                'except' => [
                    'create',
                    'store',
                    'show',
                ],
            ]
        );
    }
);

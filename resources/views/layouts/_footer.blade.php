<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 20/02/18
 * Time: 20:17
 */
?>
<div class="footer">
    <div class="footer-top">
        <div class="wrap">

        </div>
    </div>
    <div class="footer-bottom">
        <div class="wrap">
            <div class="copyrights col-md-6">
                <p> © 2015 Express News. All Rights Reserved | Design by <a href="http://w3layouts.com/">
                        W3layouts</a>
                </p>
            </div>
            <div class="footer-social-icons col-md-6">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 20/02/18
 * Time: 20:11
 */
/** @var \App\Models\News[] $popularNews */
/** @var \App\Models\News[] $lastNews */
?>
<div class="col-md-4 side-bar">
    <div class="first_half">
        <div class="list_vertical">
            <section class="accordation_menu">
                <div>
                    <input id="label-1" name="lida" checked="" type="radio">
                    <label for="label-1" id="item1">
                        <i class="ferme"> </i>Последние новости
                        <i class="icon-plus-sign i-right1"></i>
                        <i class="icon-minus-sign i-right2"></i>
                    </label>
                    <div class="content" id="a1">
                        <div class="scrollbar" id="style-2">
                            <div class="force-overflow">
                                @foreach($lastNews as $news)
                                    <div class="popular-post-grid">
                                        <div class="post-img">
                                            <a href="{{ route('home.show', $news) }}">
                                                <img src="{{ asset($news->getImageUrl()) }}" alt="">
                                            </a>
                                        </div>
                                        <div class="post-text">
                                            <a class="pp-title" href="{{ asset($news->getImageUrl()) }}">
                                                {{ $news->subject }}
                                            </a>
                                            <p>
                                                {{ $news->published_at }}
                                                <a class="span_link" href="#">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                    {{ $news->views }}
                                                </a>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <input id="label-2" name="lida" type="radio">
                    <label for="label-2" id="item2"><i class="icon-leaf" id="i2"></i>
                        Популярные новости
                        <i class="icon-plus-sign i-right1"></i>
                        <i class="icon-minus-sign i-right2"></i>
                    </label>
                    <div class="content" id="a2">
                        <div class="scrollbar" id="style-2">
                            <div class="force-overflow">
                                <div class="popular-post-grids">
                                    @foreach($popularNews as $news)
                                        <div class="popular-post-grid">
                                            <div class="post-img">
                                                <a href="{{ route('home.show', $news) }}">
                                                    <img src="{{ asset($news->getImageUrl()) }}" alt="">
                                                </a>
                                            </div>
                                            <div class="post-text">
                                                <a class="pp-title" href="{{ asset($news->getImageUrl()) }}">
                                                    {{ $news->subject }}
                                                </a>
                                                <p>
                                                    {{ $news->published_at }}
                                                    <a class="span_link" href="#">
                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                        {{ $news->views }}
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="clearfix"></div>
</div>


<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 23/02/18
 * Time: 12:24
 */
?>

@extends('layouts.app')
@section('content')
    <div class="col-md-8 content-left">
        <div class="articles">
            <header>
                <h3 class="title-head">Обратная связь</h3>
            </header>
            <div class="coment-form">
                <h4>Добавьте комментарий</h4>

                {!!  Form::open(['route' => 'guest-book.store', 'method' => 'post']) !!}

                @include('input-validation', [
                    'attribute' => 'username',
                    'input' => Form::input('text', 'username', old('username'), [
                        'placeholder' => 'Введите ваше имя',
                    ]),
                ])

                @include('input-validation', [
                        'attribute' => 'email',
                        'input' => Form::email('email', old('email'), [
                            'placeholder' => 'Введите вашу электронную почту',
                    ]),
                ])

                @include('input-validation', [
                        'attribute' => 'text',
                        'input' => Form::textarea('text', old('text'), [
                            'placeholder' => 'Введите текст сообщения',
                    ]),
                ])

                <div class="form-group">
                    <p>
                        <img src="{{ captcha_src() }}" alt="captcha" class="captcha-img" data-refresh-config="default">
                        <a href="#" id="refresh">
                        <span class="glyphicon glyphicon-refresh">
                        </span>
                        </a>
                    </p>
                </div>

                @include('input-validation', [
                        'attribute' => 'captcha',
                        'input' => Form::input('text', 'captcha', null, [
                            'placeholder' => 'Введите текст с картинки',
                    ]),
                ])

                {{ Form::submit('Разместить отзыв') }}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
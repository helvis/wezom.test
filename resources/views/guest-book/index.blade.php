<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 23/02/18
 * Time: 12:24
 */
/** @var \App\Models\GuestBook[] $message */
?>

@extends('layouts.app')
@section('content')
    <div class="col-md-8 content-left">
        <div class="articles">
            <header>
                <h3 class="title-head">Обратная связь</h3>
            </header>
            <div class="row">
                <div class="coment-form">
                    <div class="col-md-4">
                        <a href="{{ route('guest-book.create') }}" class="btn" type="button">Добавить комментарий</a>
                    </div>
                    <div class="col-md-8 text-right">
                        {{ $messages->links() }}

                    </div>
                </div>
            </div>
            @foreach($messages as $message)
                <div class="single-post">
                    <div class="newsletter">
                        <div class="media response-info">
                            <div class="media-left response-text-left">
                                {{ Html::mailto($message->mail, $message->username) }}
                            </div>
                            <div class="media-body response-text-right">
                                <p>
                                    {{ $message->text }}
                                </p>
                                <ul>
                                    <li>{{ $message->updated_at }}</li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="text-right">
                {{ $messages->links() }}
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

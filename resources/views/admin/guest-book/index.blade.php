<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 18:06
 */

/** @var \App\Models\GuestBook[] $messages */
?>
@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item active">Список сообщений</li>
        </ol>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                Список сообщений
            </div>
            <div class="card-body">
                <div class="box">
                    <div class="box-body">
                        {{ $messages->links() }}
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Добавлено</th>
                                <th>Никнейм</th>
                                <th>Почтовый адрес</th>
                                <th class="col-6">Текст сообщения</th>
                                <th>Управление</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($messages as $message)
                                <tr>
                                    <td>{{ $message->created_at }}</td>
                                    <td>{{ $message->username }}</td>
                                    <td>{{ $message->email }}</td>
                                    <td>{{ $message->text }}</td>
                                    <td>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['guest-book.destroy', $message->id]]) !!}
                                        <a href="{{ route('guest-book.edit', $message->id) }}" class="fa fa-pencil"></a>
                                        {{ Form::button('<i class="fa fa-remove text-danger"></i>', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-link',
                                            'onClick' => 'return confirm("Вы уверены?")'
                                         ]) }}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $messages->links() }}
                    </div>
                </div>
            </div>
            <div class="card-footer small text-muted"></div>
        </div>
    </section>
@endsection


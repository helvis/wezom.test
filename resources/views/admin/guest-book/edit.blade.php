<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 3:13
 */

/** @var \App\Models\GuestBook $feedBack */
?>

@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('guest-book.index') }}">Гостевая книга</a>
            </li>
            <li class="breadcrumb-item active">Создать</li>
        </ol>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                Редактирование отзыва
            </div>
            <div class="card-body">
                <div class="box">
                    <div class="box-body">
                        {!! Form::open(['id' => 'feedback-form', 'route' => ['guest-book.update', $feedBack->id], 'method' => 'put']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                @include('input-validation', [
                                'attribute' => 'username',
                                'label' => 'Имя пользователя',
                                'input' => Form::input('text', 'username', $feedBack->username, [
                                    'class' => 'form-control',
                                    ]),
                                ])
                            </div>
                            <div class="col-md-6">
                                @include('input-validation', [
                                'attribute' => 'email',
                                'label' => 'Электронная почта',
                                'input' => Form::email('email', $feedBack->email, [
                                    'class' => 'form-control',
                                    ]),
                                ])
                            </div>
                            <div class="col-md-12">
                                @include('input-validation', [
                                'attribute' => 'text',
                                'label' => 'Сообщение',
                                'input' => Form::textarea('text', $feedBack->text, [
                                    'class' => 'form-control',
                                    ]),
                                ])
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="box-footer">
                        <p>

                            <a href="{{ route('guest-book.index') }}" class="btn btn-default">Назад</a>
                            {!! Form::submit('Изменить', ['class' => 'btn btn-primary', 'form' => 'feedback-form']) !!}
                            {{--{!! Form::open(['method' => 'DELETE', 'route' => ['guest-book.destroy', $feedBack->id]]) !!}--}}
                            {{--{{ Form::button('Удалить', [--}}
                                {{--'type' => 'submit',--}}
                                {{--'class' => 'btn btn-danger pull-right',--}}
                                {{--'onClick' => 'return confirm("Вы уверены?")'--}}
                             {{--]) }}--}}
                            {{--{!! Form::close() !!}--}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


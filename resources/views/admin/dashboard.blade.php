@extends('admin.layout')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Админка</a>
        </li>
        <li class="breadcrumb-item active">Панель приборов</li>
    </ol>
    <div class="card">
        <div class="card-header">
            <i class="fa fa-area-chart"></i> Статистика</div>
        <div class="card-body">
        </div>
        <div class="card-footer small text-muted"></div>
    </div>
@endsection
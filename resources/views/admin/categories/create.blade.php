<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 17.02.2018
 * Time: 22:33
 */

/** @var \Illuminate\Support\MessageBag $errors */
?>
@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('categories.index') }}">Категории новостей</a>
            </li>
            <li class="breadcrumb-item active">Создать</li>
        </ol>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                Добавление категории
            </div>
            <div class="card-body">
                <div class="box">
                    {!! Form::open(['route' => 'categories.store']) !!}
                    <div class="box-body">
                        <div class="col-md-12">
                            @include('input-validation', [
                            'attribute' => 'title',
                            'label' => 'Имя категории',
                            'input' => Form::input('text', 'title', old('title'), [
                                'class' => 'form-control',
                                'placeholder' => 'Введите название категории новостей',
                                ]),
                            ])
                            <label>
                                <strong>
                                    Скрыть категорию из списка?
                                </strong>
                                {!!
                                Form::hidden('is_hidden', \App\Models\NewsCategory::IS_VISIBLE)
                                 . Form::checkbox('is_hidden', \App\Models\NewsCategory::IS_HIDDEN, old('is_hidden'))
                                !!}
                            </label>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ route('categories.index') }}" class="btn btn-default">Назад</a>
                        {!! Form::submit('Добавить', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection

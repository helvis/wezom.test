@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item active">Категории новостей</li>
        </ol>
    </section>
    @if(session()->has('message'))
        <p class="alert {{ session()->get('alert') }}">{{ session()->get('message') }}</p>
    @endif
    <section class="content">
        <div class="card">
            <div class="card-header">
                Список категорий новостей
            </div>
            <div class="card-body">
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <a href="{{ route('categories.create') }}" class="btn btn-success">Добавить</a>
                        </div>
                        {{ $categories->links() }}
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th class="col-md-11">Имя категории</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>
                                        <i class="fa fa-eye {!! $category->isVisible() ? 'text-success' : 'text-danger' !!}"></i>
                                    </td>
                                    <td>{{ $category->title }}</td>
                                    <td>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy', $category->id]]) !!}
                                        <a href="{{ route('categories.edit', $category->id) }}"
                                           class="fa fa-pencil"></a>
                                        {{ Form::button('<i class="fa fa-remove text-danger"></i>', [
                                        'type' => 'submit',
                                         'class' => 'btn btn-link',
                                         'onClick' => 'return confirm("Вы уверены?")'
                                         ]) }}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

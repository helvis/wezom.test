<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 3:13
 */

/** @var \App\Models\NewsCategory $category */
?>

@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('categories.index') }}">Категории новостей</a>
            </li>
            <li class="breadcrumb-item active">Создать</li>
        </ol>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                Редактирование категории
            </div>
            <div class="card-body">
                <div class="box">
                    {!! Form::open(['route' => ['categories.update', $category], 'method' => 'put']) !!}
                    <div class="box-body">
                        <div class="col-md-12">
                            @include('input-validation', [
                            'attribute' => 'title',
                            'label' => 'Имя категории',
                            'input' => Form::input('text', 'title', $category->title, [
                                'class' => 'form-control',
                                'placeholder' => 'Введите название категории новостей',
                                ]),
                            ])
                            <label>
                                <strong>
                                    Скрыть категорию из списка?
                                </strong>
                                {!!
                                Form::hidden('is_hidden', \App\Models\NewsCategory::IS_VISIBLE)
                                 . Form::checkbox('is_hidden', \App\Models\NewsCategory::IS_HIDDEN, $category->is_hidden)
                                !!}
                            </label>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ route('categories.index') }}" class="btn btn-default">Назад</a>
                        {!! Form::submit('Изменить', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection


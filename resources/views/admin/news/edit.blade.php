<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 18:06
 */

/** @var \App\Models\News $news */
?>
@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('news.index') }}">Список новостей</a>
            </li>
            <li class="breadcrumb-item active">Изменение новости #{{ $news->id }}</li>
        </ol>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                Редактирование новости
            </div>
            <div class="card-body">
                <div class="box">
                    {!! Form::open([
                        'route' => ['news.update', $news],
                        'files' => true,
                        'method' => 'put',
                    ]) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('input-validation', [
                                    'attribute' => 'subject',
                                    'label' => 'Тема',
                                    'input' => Form::text('subject', $news->subject, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Введите тему новости',
                                        ]),
                                ])
                            </div>
                            <div class="col-md-6">
                                @include('input-validation', [
                                    'attribute' => 'category_id',
                                    'label' => 'Тема',
                                    'input' => Form::select('category_id', $categories, $news->category_id,
                                    ['class' => 'form-control',])
                                ])
                            </div>
                            <div class="col-md-6">
                                @include('input-validation', [
                                    'attribute' => 'published_at',
                                    'label' => 'Дата опубликования',
                                    'input' => Form::date('published_at', \Carbon\Carbon::parse($news->published_at),
                                    ['class' => 'form-control',])
                                ])
                            </div>
                            <div class="col-md-12">
                                @include('input-validation', [
                                    'attribute' => 'content',
                                    'label' => 'Текст новости',
                                    'input' => Form::textarea('content', $news->content, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Введите текст новости',
                                        ]),
                                ])
                            </div>
                            <div class="col-md-12">
                                <label>
                                    <strong>
                                        Скрыть новость из списка?
                                    </strong>
                                    {!!
                                    Form::hidden('is_hidden', \App\Models\News::IS_VISIBLE)
                                     . Form::checkbox('is_hidden', \App\Models\News::IS_HIDDEN, old('is_hidden'))
                                    !!}
                                </label>
                            </div>
                            <div class="col-md-12">
                            <hr class="mt-2">
                                <img src="{{ $news->getImageUrl() }}" alt="" class="img-responsive" width="200">
                                <hr class="mt-2">
                                @include('input-validation', [
                                    'attribute' => 'image',
                                    'label' => 'Добавить изображение',
                                    'input' => Form::file('image', [
                                    ]),
                                ])
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ route('news.index') }}" class="btn btn-default">Назад</a>
                        {!! Form::submit('Изменить', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection



<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 18:06
 */
?>
@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('news.index') }}">Список новостей</a>
            </li>
            <li class="breadcrumb-item active">Новая новсть</li>
        </ol>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                Создание новости
            </div>
            <div class="card-body">
                <div class="box">
                    {!! Form::open([
                        'route' => 'news.store',
                        'files' => true,
                    ]) !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('input-validation', [
                                    'attribute' => 'subject',
                                    'label' => 'Тема',
                                    'input' => Form::text('subject', old('subject'), [
                                        'class' => 'form-control',
                                        'placeholder' => 'Введите тему новости',
                                        ]),
                                ])
                            </div>
                            <div class="col-md-6">

                                @include('input-validation', [
                                    'attribute' => 'category_id',
                                    'label' => 'Тема',
                                    'input' => Form::select('category_id', $categories, old('category_id'),
                                    ['class' => 'form-control',])
                                ])
                            </div>
                            <div class="col-md-6">
                                @include('input-validation', [
                                    'attribute' => 'published_at',
                                    'label' => 'Дата опубликования',
                                    'input' => Form::date('published_at', \Carbon\Carbon::now(),
                                    ['class' => 'form-control',])
                                ])
                            </div>
                            <div class="col-md-12">
                                @include('input-validation', [
                                    'attribute' => 'content',
                                    'label' => 'Текст новости',
                                    'input' => Form::textarea('content', old('content'), [
                                        'class' => 'form-control',
                                        'placeholder' => 'Введите текст новости',
                                        ]),
                                ])
                            </div>
                            <div class="col-md-12">
                                <label>
                                    <strong>
                                        Скрыть новость из списка?
                                    </strong>
                                    {!!
                                    Form::hidden('is_hidden', \App\Models\News::IS_VISIBLE)
                                     . Form::checkbox('is_hidden', \App\Models\News::IS_HIDDEN, old('is_hidden'))
                                    !!}
                                </label>
                            </div>
                            <div class="col-md-12">
                                <hr class="mt-2">
                                @include('input-validation', [
                                    'attribute' => 'image',
                                    'label' => 'Добавить изображение',
                                    'input' => Form::file('image', [
                                    ]),
                                ])
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ route('news.index') }}" class="btn btn-default">Назад</a>
                        {!! Form::submit('Добавить', ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection


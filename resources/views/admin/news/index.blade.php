<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 18:06
 */
?>
@extends('admin.layout')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Админка</a>
            </li>
            <li class="breadcrumb-item active">Список новостей</li>
        </ol>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header">
                Новости
            </div>
            <div class="card-body">
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <a href="{{ route('news.create') }}" class="btn btn-success">Добавить</a>
                        </div>
                        {{ $newsCollection->links() }}
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Создана</th>
                                <th>Опубликована</th>
                                <th>Категория</th>
                                <th>Тема</th>
                                <th class="col-6">Текст</th>
                                <th>Изображение</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newsCollection as $news)
                                <tr>
                                    <td>
                                        <i class="fa fa-eye {!! $news->isVisible() ? 'text-success' : 'text-danger' !!}"></i>
                                    </td>
                                    <td>{{ $news->created_at }}</td>
                                    <td>{{ $news->published_at }}</td>
                                    <td>{{ $news->category->title }}</td>
                                    <td>{{ $news->subject }}</td>
                                    <td>{{ $news->getShortContent() }}</td>
                                    <td><img src="{{ asset($news->getImageUrl()) }}" alt="" width="100"></td>
                                    <td>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['news.destroy', $news->id]]) !!}
                                        <a href="{{ route('news.edit', $news->id) }}" class="fa fa-pencil"></a>
                                        {{ Form::button('<i class="fa fa-remove text-danger"></i>', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-link',
                                            'onClick' => 'return confirm("Вы уверены?")'
                                         ]) }}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $newsCollection->links() }}
                    </div>
                </div>
            </div>
            <div class="card-footer small text-muted"></div>
        </div>
    </section>
@endsection


<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 15/02/18
 * Time: 13:00
 */
?>

<ul class="navbar-nav navbar-sidenav">
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
        <a class="nav-link" href="/">
            <i class="fa fa-fw fa-home"></i>
            <span class="nav-link-text">На главную</span>
        </a>
    </li>
    <li class="nav-item {{ Route::currentRouteNamed('news.*') ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
        <a class="nav-link" href="{{ route('news.index') }}">
            <i class="fa fa-fw fa-newspaper-o"></i>
            <span class="nav-link-text">Новости</span>
        </a>
    </li>
    <li class="nav-item {{ Route::currentRouteNamed('categories.*') ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
        <a class="nav-link" href="{{ route('categories.index') }}">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Категории новостей</span>
        </a>
    </li>
    <li class="nav-item {{ Route::currentRouteNamed('guest-book.*') ? 'active' : '' }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
        <a class="nav-link" href="{{ route('guest-book.index') }}">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Гостевая книга</span>
        </a>
    </li>
</ul>


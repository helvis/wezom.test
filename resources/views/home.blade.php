<?php
/** @var \Illuminate\Pagination\LengthAwarePaginator $newsCollection */
?>
@extends('layouts.app')
@section('content')
    <div class="col-md-8 content-left">
        <div class="articles">
            <header>
                <h3 class="title-head">Новости мира</h3>
            </header>
            {{ $newsCollection->links() }}
            @foreach($newsCollection as $news)
                @php($urlForThisNews = route('home.show', $news->id))
                <div class="article">
                    <div class="article-left">
                        <a href="{{ $urlForThisNews }}">
                            <img src="{{ $news->getImageUrl() }}" alt="" class="img-responsive" width="200">
                        </a>
                    </div>
                    <div class="article-right">
                        <div class="article-title">
                            <p> {{ $news->published_at }}
                                <a class="span_link" href="#">
                                <span class="glyphicon glyphicon-eye-open">
                                </span> {{ $news->views }}
                                </a>
                            </p>
                            <a class="title" href="{{ $urlForThisNews }}">
                                {{ $news->subject }}
                            </a>
                        </div>
                        <div class="article-text">
                            <p>
                                {{ $news->getShortContent() }}
                            </p>
                            <a href="{{ $urlForThisNews }}"><img src="{{ asset('images/more.png') }}" alt=""></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
            {{ $newsCollection->links() }}
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

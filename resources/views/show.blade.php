<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 21/02/18
 * Time: 17:38
 */
/** @var \App\Models\News $news */
?>
@extends('layouts.app')
@section('content')
    <div class="col-md-2 share_grid">
        <h3>SHARE</h3>
        <ul>
            <li>
                <a href="#">
                    <i class="facebook"></i>
                    <div class="views">
                        <span>SHARE</span>
                        <label>180</label>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="twitter"></i>
                    <div class="views">
                        <span>TWEET</span>
                        <label>355</label>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="linkedin"></i>
                    <div class="views">
                        <span>SHARES</span>
                        <label>28</label>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="pinterest"></i>
                    <div class="views">
                        <span>PIN</span>
                        <label>16</label>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="email"></i>
                    <div class="views">
                        <span>Email</span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-6 content-left single-post">
        <div class="blog-posts">
            <h3 class="post">{{ $news->subject }}</h3>
            <img src="{{ asset($news->getImageUrl()) }}" class="img-responsive">
            <div class="last-article">
                <p class="artext">
                    {{ $news->content }}
                </p>
                {{--<ul class="categories">--}}
                    {{--<li><a href="#">Markets</a></li>--}}
                {{--</ul>--}}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

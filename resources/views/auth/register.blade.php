@extends('layouts.app')

@section('content')
    <div class="col-md-8 content-left">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="articles newsletter">
                        <div class="panel-body">
                            <div class="coment-form">
                                {{ Form::open([
                                    'route' => 'register',
                                    'method' => 'POST',
                                    'class' => 'form-horizontal',
                                 ]) }}
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Имя</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" name="name" value="{{ old('name') }}" required
                                               autofocus>

                                        @if ($errors->has('name'))
                                            <div class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Электронная почта</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <div class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Пароль</label>
                                    <div class="col-md-6">
                                        <input id="password" type="password" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">
                                        Подтверждение пароля
                                    </label>
                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" name="password_confirmation"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        {{ Form::submit('Зарегистрироваться') }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

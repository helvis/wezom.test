<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 24.02.2018
 * Time: 0:57
 */

namespace Tests\Feature;


use Tests\TestCase;

class HomePageTest extends TestCase
{
    public function testHomePage()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('Новости мира');
    }
}
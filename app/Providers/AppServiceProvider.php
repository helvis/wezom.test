<?php

namespace App\Providers;

use App\Models\News;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use Validator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts._sidebar', function ($view) {
            $view->with('popularNews', News::popular()
                ->published()
                ->take(10)
                ->get()
            );
            $view->with('lastNews', News::lastNews()
                ->published()
                ->take(10)
                ->get()
            );
        });

        Validator::extend(
            'hasHtmlTags',
            'App\Http\Validators\HtmlValidator@hasHtmlTags',
            'Поле не должно содержать HTML теги.'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}

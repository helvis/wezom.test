<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 22:50
 */

namespace App\Services;

use Auth;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\News;

class NewsService
{
    private $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function create(NewsRequest $request, Auth $auth): bool
    {
        $news = new News();
        $news->fill($request->all());

        $news->creator_id = $auth::id();

        $image = $request->file('image');
        if (!is_null($image)) {
            $fileName = $this->fileService->storeFile($image);
            $news->image = $fileName;
        }

        return $news->save();
    }

    public function update(NewsRequest $request, News $news): bool
    {
        $news->fill($request->all());

        $image = $request->file('image');
        if (!is_null($image)) {
            $fileName = $this->fileService->storeFile($image);
            $news->image = $fileName;
        }

        return $news->save();
    }

    /**
     * @param \App\Models\News $news
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(News $news): bool
    {
        if ($news->image) {
            $this->fileService->destroyFile($news->image);
        }

        return $news->delete();
    }
}
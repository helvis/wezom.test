<?php
/**
 * Created by PhpStorm.
 * User: Boiko Sergii
 * Date: 18.02.2018
 * Time: 23:22
 */

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileService
{
    const UPLOAD_PATH = 'public';

    const FILE_NAME_LENGTH = 16;

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return string
     */
    public function storeFile(UploadedFile $file): string
    {
        $fileName = sprintf('%s.%s', str_random(self::FILE_NAME_LENGTH), $file->extension());

        $file->storePubliclyAs(self::UPLOAD_PATH, $fileName);

        return $fileName;
    }

    public function destroyFile(string $fileName)
    {
        return Storage::delete($fileName);
    }

    public static function getFileUrl(string $fileName): string
    {
        return Storage::url($fileName);
    }

    public static function getDummyFile()
    {
        return 'images/no-image.jpg';
    }
}
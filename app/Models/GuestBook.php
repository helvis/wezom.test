<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GuestBook
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GuestBook whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GuestBook whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GuestBook whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GuestBook whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GuestBook whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GuestBook whereUsername($value)
 * @mixin \Eloquent
 */
class GuestBook extends Model
{
    public $table = 'guest_book';

    public $fillable = [
        'username',
        'email',
        'text',
    ];

    public function scopeGetLastEdited($query)
    {
        return $query->orderBy('updated_at', 'desc');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NewsCategory
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property int $is_hidden
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsCategory whereIsHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsCategory whereUpdatedAt($value)
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class NewsCategory extends Model
{
    const IS_VISIBLE = 0;

    const IS_HIDDEN = 1;

    protected $fillable = [
        'title',
        'is_hidden',
    ];

    protected $attributes = [
        'is_hidden' => self::IS_VISIBLE,
    ];

    public function news()
    {
        return $this->hasMany(News::class, 'category_id', 'id');
    }

    public function isVisible()
    {
        return $this->is_hidden == self::IS_VISIBLE;
    }

    public function scopeGetLastEdited($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}

<?php

namespace App\Models;

use App\Services\FileService;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\News
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $category_id
 * @property string $subject
 * @property string $content
 * @property string $image
 * @property int $is_hidden
 * @property int $views
 * @property int $creator_id
 * @property \Carbon\Carbon|null $published_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereIsHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 */
class News extends Model
{
    const IS_VISIBLE = 0;

    const IS_HIDDEN = 1;

    const IMAGE_PATH = 'uploads';

    public $fillable = [
        'category_id',
        'subject',
        'content',
        'is_hidden',
    ];

    public function category()
    {
        return $this->hasOne(NewsCategory::class, 'id', 'category_id');
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function isVisible()
    {
        return $this->is_hidden === self::IS_VISIBLE;
    }

    public function getImage()
    {
        if ($this->image) {
            return sprintf('/%s/%s', self::IMAGE_PATH, $this->image);
        }

        return '';
    }

    public function getShortContent(int $trimWidth = 180): string
    {
        $string = mb_strimwidth($this->content, 0, $trimWidth);

        if (mb_strwidth($this->content) > $trimWidth) {
            $string .= '. . .';
        }

        return $string;
    }

    public function getImageUrl(): string
    {
        if (empty($this->image)) {
            return FileService::getDummyFile();
        }

        return FileService::getFileUrl($this->image);
    }

    public function setViewed(): void
    {
        $this->views++;
        $this->save();
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<', Carbon::now());
    }

    public function scopePopular($query)
    {
        return $query->orderBy('views', 'desc');
    }

    public function scopeLastNews($query)
    {
        return $query->orderBy('published_at', 'desc');
    }

    public static function scopeVisible($query)
    {
        return $query->where(['is_hidden' => self::IS_VISIBLE]);
    }

    public function scopeGetLastEdited($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}

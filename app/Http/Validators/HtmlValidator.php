<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 22/02/18
 * Time: 19:37
 */

namespace App\Http\Validators;


class HtmlValidator
{
    public function hasHtmlTags($attribute, $value, $parameters, $validator)
    {
        return $value === strip_tags($value);
    }
}
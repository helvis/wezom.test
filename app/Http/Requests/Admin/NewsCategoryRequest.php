<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NewsCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $id = $this->category->id ?? null;

        return [
            'title' => [
                'required',
                Rule::unique('news_categories')->ignore($id),
                'max:255',
            ],
            'is_hidden' => 'integer',
        ];
    }
}

<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    private $creator_id;

    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        if (is_null($this->creator_id)) {
            $this->creator_id = \Auth::id();
        }

        return [
            'subject' => 'required|max:255',
            'category_id' => 'required|exists:news_categories,id',
            'published_at' => 'required|date',
            'is_hidden' => 'integer',
            'content' => 'required',
            'image' => 'nullable|image',
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: box
 * Date: 21/02/18
 * Time: 19:49
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|alpha_num',
            'email' => 'required|email',
            'text' => 'required|hasHtmlTags',
            'captcha' => 'required|captcha',
        ];
    }
}
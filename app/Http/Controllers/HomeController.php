<?php

namespace App\Http\Controllers;

use App\Models\News;

class HomeController extends Controller
{
    const PER_PAGE = 5;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsCollection = News::with('category')
            ->lastNews()
            ->published()
            ->visible()
            ->paginate(self::PER_PAGE);

        return view('home', [
            'newsCollection' => $newsCollection,
        ]);
    }

    public function show(int $id)
    {
        /** @var News $news */
        $news = News::findOrFail($id);
        $news->setViewed();

        return view('show', [
            'news' => $news,
        ]);
    }
}

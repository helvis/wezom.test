<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Models\GuestBook;

class GuestBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = GuestBook::getLastEdited()
            ->paginate();

        return view('guest-book.index', [
            'messages' => $messages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guest-book.create');
    }

    /**
     * @param FeedbackRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FeedbackRequest $request)
    {
        GuestBook::create($request->all());

        return redirect()->route('guest-book');
    }
}

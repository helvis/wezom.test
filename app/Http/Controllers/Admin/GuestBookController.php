<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FeedbackRequest;
use App\Models\GuestBook;
use App\Http\Controllers\Controller;

class GuestBookController extends Controller
{
    const PER_PAGE = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = GuestBook::getLastEdited()
            ->paginate(self::PER_PAGE);

        return view('admin.guest-book.index', [
            'messages' => $messages,
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $feedBack = GuestBook::findOrFail($id);

        return view('admin.guest-book.edit', [
            'feedBack' => $feedBack,
        ]);
    }

    /**
     * @param FeedbackRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FeedbackRequest $request, int $id)
    {
        $feedBack = GuestBook::findOrFail($id);
        $feedBack->update($request->all());

        return redirect()->route('guest-book.index');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $feedBack = GuestBook::findOrFail($id);
        $feedBack->delete();

        return redirect()->route('guest-book.index');
    }
}

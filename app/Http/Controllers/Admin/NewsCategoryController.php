<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsCategoryRequest;
use App\Models\NewsCategory;
use Illuminate\Http\Request;

class NewsCategoryController extends Controller
{
    const PER_PAGE = 15;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = NewsCategory::getLastEdited()
            ->paginate(self::PER_PAGE);

        return view('admin.categories.index', [
            'categories' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * @param NewsCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewsCategoryRequest $request)
    {
        NewsCategory::create($request->all());

        return redirect()->route('categories.index');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $category = NewsCategory::findOrFail($id);

        return view('admin.categories.edit', [
            'category' => $category,
        ]);
    }

    /**
     * @param \App\Http\Requests\Admin\NewsCategoryRequest $request
     * @param \App\Models\NewsCategory $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewsCategoryRequest $request, NewsCategory $category)
    {
        $category->update($request->all());

        return redirect()->route('categories.index');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        /** @var NewsCategory $category */
        $category = NewsCategory::findOrFail($id);

        $newsCount = $category->news()->count();

        if ($newsCount) {
            session()->flash('message', sprintf('This category used in %s news', $newsCount));
            session()->flash('alert', 'alert-danger');
        } else {
            $category->delete();
        }

        return redirect()->route('categories.index');
    }
}

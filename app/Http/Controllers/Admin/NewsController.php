<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\News;
use App\Models\NewsCategory;
use App\Services\NewsService;

class NewsController extends Controller
{
    const PER_PAGE = 15;

    private $newsService;

    /**
     * NewsController constructor.
     *
     * @param \App\Services\NewsService $newsService
     */
    public function __construct(NewsService $newsService)
    {
        $this->middleware('auth');
        $this->newsService = $newsService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $newsCollection = News::with('category')
            ->getLastEdited()
            ->paginate(self::PER_PAGE);

        return view('admin.news.index', [
            'newsCollection' => $newsCollection,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.news.create', [
            'categories' => NewsCategory::pluck('title', 'id')->all(),
        ]);
    }

    /**
     * @param \App\Http\Requests\Admin\NewsRequest $request
     * @param \Auth $auth
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewsRequest $request, Auth $auth)
    {
        $this->newsService->create($request, $auth);

        return redirect()->route('news.index');
    }

    /**
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(News $news)
    {
        return view('admin.news.edit', [
            'news' => $news,
            'categories' => NewsCategory::pluck('title', 'id')->all(),
        ]);
    }

    /**
     * @param NewsRequest $request
     * @param News $news
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewsRequest $request, News $news)
    {
        $this->newsService->update($request, $news);

        return redirect()->route('news.index');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        /** @var News $news */
        $news = News::findOrFail($id);
        $this->newsService->destroy($news);

        return redirect()->route('news.index');
    }
}
